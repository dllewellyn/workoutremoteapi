# WorkoutGenerator
## Introduction

WorkoutGenerator is intended to provide a standard way of creating workout plans, and a standard API for accessing this data. 

## Installation of pre-reqs

The pre-reqs for WorkoutGenerator (aside from python) are listed below:

pip install -r requirements.txt

## Running

To start, create a file in workoutgeneratorapi/workoutviewer/workoutviewer/secrets.py

Make it look like this:

class Secrets(object):

  @staticmethod
  def get_secrets():
    return {"secret_key": "", 
    "allowed_hosts": []}

Allowed host should be your host(s), a comma seperated string array (leave blank if you're just running locally) and the secret key is a long key used by django

execute 

python manage.py migrate
python manage.py createsuperuser
python manage.py runserver

The web pages that you would use are:

docs/ (restful API docs)
admin/ (create your models, login with super user)
generator/ (for the API. See reference linked below for full API information)


## Development overview

The application makes use of django for web hosting and django rest framework in order to provide a RESTful API to access data stored on the website.
