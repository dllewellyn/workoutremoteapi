__author__ = 'danielllewellyn'

from django.test import TestCase
from GenerateTestData import GenerateData
from django.test import Client
from django.core.urlresolvers import reverse
import urllib

class RestAPIListsTest(TestCase):

    def setUp(self):
        generator = GenerateData()
        generator.generator()

    def test_retrieve_workout_lists(self):
        client = Client()
        response = client.get(reverse("lists"))
        assert response.status_code == 200
        assert response.data[0]["name"] == GenerateData.WORKOUT_LIST_NAME

    def test_retrieve_schedule__for_date_gives_a_day(self):
        client = Client()
        start_date = "01-01-90"
        workout_name = GenerateData.WORKOUT_LIST_NAME
        response = client.get(reverse("schedule_date", args=[workout_name, start_date]))
        assert response.status_code == 200
        assert response.data["workouts"]["01/01/90"]["workout_name"] == GenerateData.WORKOUT_NAME
        assert response.data["dates"][0] == "01/01/90"