__author__ = 'danielllewellyn'

from WorkoutGenerator.models import *
import datetime

class GenerateData(object):

    WORKOUT_LIST_NAME = "Basic workout"

    WORKOUT_NAME = "Run"

    def generator(self):
        intensity5 = Intensity(intensity_description="Maximum intensity", intensity_name="5")
        intensity5.save()
        intensity1 = Intensity(intensity_description="Minimum intensity", intensity_name="1")
        intensity1.save()
        intensity4 = Intensity(intensity_description="Economy", intensity_name="4")
        intensity4.save()
        intensity3 = Intensity(intensity_description="Stamina", intensity_name="3")
        intensity3.save()
        intensity2 = Intensity(intensity_description="Endurance", intensity_name="2")
        intensity2.save()

        constant_run_step = Step(intensity=intensity2, description="Run at a constant pace", time_in_zone=45)
        constant_run_step.save()

        constant_run_list = StepList(name="Run")
        constant_run_list.save()

        step_list = StepListLink(step_list=constant_run_list, step=constant_run_step, order_of_step=1)
        step_list.save()

        workout = Workout(overall_intensity=intensity2,
                          total_time=60,
                          warm_up=10,
                          cooldown=5,
                          workout_name=GenerateData.WORKOUT_NAME)
        workout.save()

        constant_run_step_list_link = StepWorkoutListLink(workout=workout, step_list=constant_run_list, order=1, repetitions=1)
        constant_run_step_list_link.save()

        workout_list = WorkoutList(start_date=datetime.datetime.now(),
                                   duration=14,
                                   name=GenerateData.WORKOUT_LIST_NAME,
                                   description="A long winded description should go here",
                                   image=None,
                                   author="Daniel Llewellyn")
        workout_list.save()

        workout_list_link = WorkoutListLink(workout_list=workout_list, workout=workout, days_into_workout=1)
        workout_list_link.save()