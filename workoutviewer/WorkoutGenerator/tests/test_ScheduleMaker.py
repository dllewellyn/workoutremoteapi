__author__ = 'danielllewellyn'

from django.test import TestCase
from WorkoutGenerator.ScheduleMaker import ScheduleMaker
from WorkoutGenerator.models import Workout, Intensity, WorkoutList, WorkoutListLink

from datetime import datetime

duration = 14


class TestScheduleMaker(TestCase):
    def setUp(self):
        intensity = Intensity(intensity_description="blahblah", intensity_name="aaa")
        workout1 = Workout(overall_intensity=intensity, warm_up=10, cooldown=10, workout_name="testname", total_time=30)
        workout2 = Workout(overall_intensity=intensity, warm_up=10, cooldown=10, workout_name="testname 2", total_time=30)
        workout3 = Workout(overall_intensity=intensity, warm_up=10, cooldown=10, workout_name="testname 3", total_time=30)
        workout4 = Workout(overall_intensity=intensity, warm_up=10, cooldown=10, workout_name="testname 4", total_time=30)
        workout5 = Workout(overall_intensity=intensity, warm_up=10, cooldown=10, workout_name="testname 5", total_time=30)
        workout6 = Workout(overall_intensity=intensity, warm_up=10, cooldown=10, workout_name="testname 6", total_time=30)

        start_date = datetime.now()

        name = "test list"
        description = "The description"

        wo_list = WorkoutList(start_date=start_date, duration=duration, name=name, description=description, author="Test")

        workout1.save()
        workout2.save()
        workout3.save()
        workout4.save()
        workout5.save()
        workout6.save()
        wo_list.save()

        wo_list_link = WorkoutListLink(workout=workout1, workout_list=wo_list, days_into_workout=1)
        wo_list_link.save()

        wo_list_link_2 = WorkoutListLink(workout=workout2, workout_list=wo_list, days_into_workout=3)
        wo_list_link_2.save()

        wo_list_link_3 = WorkoutListLink(workout=workout3, workout_list=wo_list, days_into_workout=5)
        wo_list_link_3.save()

        wo_list_link_4 = WorkoutListLink(workout=workout4, workout_list=wo_list, days_into_workout=7)
        wo_list_link_4.save()

        wo_list_link_5 = WorkoutListLink(workout=workout5, workout_list=wo_list, days_into_workout=9)
        wo_list_link_5.save()

        wo_list_link_6 = WorkoutListLink(workout=workout6, workout_list=wo_list, days_into_workout=11)
        wo_list_link_6.save()
        all_items = WorkoutListLink.objects.all()

        self.schedule_maker = ScheduleMaker(all_items)


    def test_generating_a_list_returns_a_full_list(self):
        list = self.schedule_maker.__generate_full_array__()

        assert len(list) == duration
        assert list[0].workout_name == "testname"
        assert list[2].workout_name == "testname 2"

    def test_that_the_correct_number_of_days_is_returned_from_get_weeks(self):
        assert self.schedule_maker.weeks == 2

    def test_that_we_can_get_super_dict(self):
        result = self.schedule_maker.arrange_list_into_weeks()
        assert len(result["1"]) == 7
        assert len(result["2"]) == 7
        assert result["1"][0].workout_name == "testname"
        assert result["1"][2].workout_name == "testname 2"
        assert result["1"][4].workout_name == "testname 3"
        assert result["1"][6].workout_name == "testname 4"
        assert result["2"][1].workout_name == "testname 5"
        assert result["2"][3].workout_name == "testname 6"

    def test_that_we_can_get_the_correct_statistics_for_number_of_weeks(self):
        result = self.schedule_maker.get_weekly_time_stats()
        res1, a, b = result
        assert res1["1"] == 120

    def test_that_we_can_get_a_workout_given_the_start_date(self):
        ret_dict, ret_array = self.schedule_maker.arrange_list_given_date("01-01-90")
        assert ret_dict["01/01/90"].workout_name == "testname"
        assert ret_dict["02/01/90"] == None
        assert ret_dict["03/01/90"].workout_name == "testname 2"

        assert ret_array[0] == "01/01/90"
        assert ret_array[3] == "04/01/90"
        assert ret_array[5] == "06/01/90"
        assert ret_array[13] == "14/01/90"

    def test_that_we_can_get_a_workout_given_the_start_date_over_a_new_month(self):
        ret_dict, ret_array = self.schedule_maker.arrange_list_given_date("30-01-90")
        assert ret_dict["30/01/90"].workout_name == "testname"
        assert ret_dict["31/01/90"] == None
        assert ret_dict["01/02/90"].workout_name == "testname 2"

        assert ret_array[0] == "30/01/90"
        assert ret_array[3] == "02/02/90"
        assert ret_array[5] == "04/02/90"
        assert ret_array[13] == "12/02/90"


    def test_that_we_can_get_a_workout_given_the_start_date_over_a_new_year(self):
        ret_dict, ret_array = self.schedule_maker.arrange_list_given_date("30-12-90")
        assert ret_dict["30/12/90"].workout_name == "testname"
        assert ret_dict["31/12/90"] == None
        assert ret_dict["01/01/91"].workout_name == "testname 2"
