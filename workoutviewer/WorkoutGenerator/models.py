from django.contrib import admin
from django.db import models
from django.contrib.auth.models import User


class Intensity(models.Model):
    """
    Intensity.
    """
    intensity_description = models.CharField(max_length=256, default="N/A")
    intensity_name = models.CharField(primary_key=True, max_length=256, default="N/A")

    def __unicode__(self):
       return 'Intensity: ' + self.intensity_description


class Step(models.Model):
    """
    A step is a step, in a step list. A step list can have more than one step, a step
    should be for example, run for 1min or cooldown 1min
    """
    intensity = models.ForeignKey(Intensity)
    description = models.CharField(max_length=256)
    time_in_zone = models.IntegerField()

    def __unicode__(self):
        return self.description


class StepList(models.Model):
    """
    Step. A collection of steps
    """
    name = models.CharField(max_length=256)
    step = models.ManyToManyField(Step, through="StepListLink")

    def __unicode__(self):
        return self.name


class Workout(models.Model):
    """
    Workout. A workout is the 'workout' in its entirety
    """
    overall_intensity = models.ForeignKey(Intensity)
    total_time = models.IntegerField()
    warm_up = models.IntegerField()
    cooldown = models.IntegerField()
    workout_name = models.CharField(max_length=256, default="N/A")
    step_lists = models.ManyToManyField('StepList', through='StepWorkoutListLink')

    def __unicode__(self):
        return self.workout_name


class Keywords(models.Model):
    """
    Keywords that we can try searching by
    """
    keyword = models.CharField(max_length=256)

    def __unicode__(self):
        return self.keyword


class WorkoutList(models.Model):
    """
    A list of workouts. A list makes up a plan.
    """
    start_date = models.DateTimeField()
    duration = models.IntegerField()
    name = models.CharField(max_length=256)
    workouts = models.ManyToManyField('Workout', through='WorkoutListLink')
    description = models.TextField()
    image = models.ImageField()
    keyword = models.ManyToManyField(Keywords)
    author = models.CharField(max_length=256)

    def __unicode__(self):
        return self.name


class WorkoutListLink(models.Model):
    """
    A list of workouts and link to a workout list
    """
    workout_list = models.ForeignKey(WorkoutList)
    workout = models.ForeignKey(Workout)
    days_into_workout = models.IntegerField()


class StepWorkoutListLink(models.Model):
    """
    Link between step list and workout
    """
    step_list = models.ForeignKey(StepList)
    workout = models.ForeignKey(Workout)
    repetitions = models.IntegerField()
    order = models.IntegerField()


class StepListLink(models.Model):
    """
    Link between step list and step
    """
    step_list = models.ForeignKey(StepList)
    step = models.ForeignKey(Step)
    order_of_step = models.IntegerField()


class WorkoutUser(models.Model):
    """
    Workout data
    """
    user = models.ForeignKey(User)
    workouts = models.ForeignKey(WorkoutList)
