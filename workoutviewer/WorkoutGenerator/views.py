
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_serializers import *
from ScheduleMaker import ScheduleMaker
from models import *

import os
from django.conf import settings


def image(request, image_name):
    """
    Image
    """
    img = open(os.path.join(settings.MEDIA_ROOT, image_name), 'rb').read()
    return HttpResponse(img, content_type="image/png")


class WorkoutListViewItem(APIView):
    """
    Workout list view item
    """

    def get(self, request, workout_name):
        workouts = WorkoutList.objects.get(name=workout_name)
        serializer = WorkoutListSerializer(workouts, many=False)
        return Response(serializer.data)


class WorkoutListViewFilter(APIView):
    """
    Workout list that filters on the keywords. Pass to the URL something similar to this:
    /query/Beginner,Triathlon to filter on Beginner and triathlon
    """

    def get(self, request, keyword_list):

        keywords = keyword_list.split(',')
        results = None

        for keyword in keywords:
            keyword_obj = Keywords.objects.filter(keyword=keyword)

            if results is None:
                results = WorkoutList.objects.filter(keyword=keyword_obj)
            else:
                results = results.filter(keyword=keyword_obj)


        serializer = WorkoutListSerializer(results, many=True)

        return Response(serializer.data)


class WorkoutListsView(APIView):
    """
    Get a list of workout plans

    parameters:

      - name: name

        description: Name of the workout plan

        type: string

    - name: duration

        description: number of days that this fitness plan will last

        type: integer

    - name: author

        description: author of the workout list

        type: string

    - name: keyword

        description: list of keywords (strings) that are associated with the workout list

        type: array

    - name: workouts

        description: a list of workouts (see workout for more info). Each workout also includes
        a 'days_into_workout' field unique to the workoutlist. This indicates when this workout should be
        done in relation to the overall plan.

        type: array
    """

    def get(self, request):
        workouts = WorkoutList.objects.all()
        serializer = WorkoutListSerializer(workouts, many=True)
        return Response(serializer.data)


class WorkoutView(APIView):
    """
    View an indiviual workout, given its name

    parameters:

        - name: workout_name

          description: name of this workout

        - name: warm_up

            description: time in mins for warmup before the workout

        -name: cooldown

        description: time in mins for cooldown at the end of the workout

        - name: overall_intensity

        description: the intensity of a workout

        - name: total_time

        description: total time for the entire workout, including warmup and cooldown

        - name: step_list

        description: a list of steps that make up the workout. See steplist for more info. In addition
        to the steplist params, it also has the field 'order' and 'repetitions' this is the order in which you
        should perform each step_list (i.e. step_list with order 1 is first, 2 is second etc) and the number of times you should
        perform each step list. E.g. if step_list_1 has a repetition of 3, repeat 3 times before proceeding to the next step_list

    """
    def get(self, request, workout_name):
        workout = Workout.objects.filter(workout_name=workout_name)
        serializer = WorkoutSerializer(workout)
        return Response(serializer.data)


class StepListView(APIView):
    def get(self, request, step_list_name):
        steps = StepList.objects.filter(name=step_list_name)
        serializer = StepListLinkSerializer(steps)
        return Response(serializer.data)


class WorkoutListScheduleStartDate(APIView):
    """
    Given a workout list and a start date, get the schedule
    """
    def get(self, request, workout_name, start_date):
        workout_list = WorkoutList.objects.filter(name=workout_name)

        workouts = WorkoutListLink.objects.filter(workout_list=workout_list)
        schedule_maker = ScheduleMaker(workouts)

        ret_dict, ret_array = schedule_maker.arrange_list_given_date(start_date)

        for item in ret_dict:
            if not ret_dict[item]:
                ret_dict[item] = "None"
            else:
                workout = WorkoutSerializer(ret_dict[item])
                ret_dict[item] = workout.data

        workout_list_serializer = WorkoutListSerializer(workout_list.get(), many=False)
        return Response({'workouts': ret_dict, 'dates': ret_array, "workout_list": workout_list_serializer.data})


class WorkoutListSchedule(APIView):

    def getNameForDay(self, day):
        """
        Get the name for a day
        :param day: day of the week
        """
        return {1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday", 6: "Friday", 7: "Saturday"}[day]

    def get(self, request, workout_name):
        workout_list = WorkoutList.objects.filter(name=workout_name)

        workouts = WorkoutListLink.objects.filter(workout_list=workout_list)
        schedule_maker = ScheduleMaker(workouts)

        json_serializer = {}
        mlist = schedule_maker.arrange_list_into_weeks()

        for obj in mlist:

            temp = {}
            i = 0
            for blah in mlist[obj]:
                i += 1

                if not blah:
                   temp[self.getNameForDay(i)] = "None"
                else:
                    workout = WorkoutSerializer(blah)
                    temp[self.getNameForDay(i)] = workout.data
                    print temp

            json_serializer[obj] = temp


        json_result, table_weeks, table_minutes = schedule_maker.get_weekly_time_stats()

        return Response({"workouts": json_serializer, "stats": {"weeks": table_weeks, "minutes": table_minutes}})