__author__ = 'danielllewellyn'

from django.conf.urls import url, patterns
from . import views

urlpatterns = patterns('',
    url(r'^lists/', views.WorkoutListsView.as_view(), name='lists'),
    url(r'^workoutlist/(?P<workout_name>[A-Za-z0-9 ]+)', views.WorkoutListViewItem.as_view(), name='specific_list'),
    url(r'^schedule/(?P<workout_name>[A-Za-z0-9 ]+)', views.WorkoutListSchedule.as_view()),
    url(r'^media/(?P<image_name>[A-Za-z0-9.\-_ ]+)', views.image),
    url(r'^query/(?P<keyword_list>[A-Za-z0-9.\-, ]+)', views.WorkoutListViewFilter.as_view()),
    url(r'^workout/(?P<workout_name>[A-Za-z0-9.\-, ]+)', views.WorkoutView.as_view()),
    url(r'^steplist/(?P<step_list_name>[A-Za-z0-9.\-, ]+)', views.StepListView.as_view()),
    url(r'^date_schedule/(?P<workout_name>[A-Za-z0-9 ]+)/(?P<start_date>[0-9]{2}[-][0-9]{2}[-][0-9]{2})', views.WorkoutListScheduleStartDate.as_view(), name="schedule_date"),

)
