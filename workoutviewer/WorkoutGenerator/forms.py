__author__ = 'danielllewellyn'

from .models import *


class WorkoutListCategoryRelationshipInline(admin.TabularInline):
    model = WorkoutListLink
    extra = 1


class WorkoutListAdmin(admin.ModelAdmin):
    inlines = (WorkoutListCategoryRelationshipInline,)


class WorkoutStepsCategoryRelationshipInline(admin.TabularInline):
    model = StepWorkoutListLink
    extra = 1


class WorkoutStepAdmin(admin.ModelAdmin):
    inlines = (WorkoutStepsCategoryRelationshipInline,)


class StepsCategoryRelationshipInline(admin.TabularInline):
    model = StepListLink
    extra = 1


class StepListLinkAdmin(admin.ModelAdmin):
    inlines = (StepsCategoryRelationshipInline,)
