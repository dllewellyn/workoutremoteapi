from django.contrib import admin
from .models import *
from forms import *


admin.site.register(Workout, WorkoutStepAdmin)
admin.site.register(WorkoutList, WorkoutListAdmin)
admin.site.register(Intensity)
admin.site.register(StepList, StepListLinkAdmin)
admin.site.register(Step)
admin.site.register(WorkoutUser)
admin.site.register(Keywords)