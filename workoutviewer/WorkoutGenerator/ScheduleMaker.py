__author__ = 'danielllewellyn'

import datetime


class ScheduleMaker(object):
    """
    Make a schedule from the WorkoutList
    """

    # Max weeks. One year? Probably enough...
    MAX_WEEKS = 52

    # Days in a week
    DAYS_IN_WEEK = 7

    def __init__(self, query_result):
        """

        :param workout_list: Pass in a workout_list (a list of workouts
        from the workouts)

        """
        self.workout_list = query_result
        self.workout_list_object = query_result[0].workout_list

    @property
    def weeks(self):
        """
        Get the number of weeks that we're going to run for
        :return: number of weeks we're runnigg for
        """
        return self.workout_list_object.duration / ScheduleMaker.DAYS_IN_WEEK

    def arrange_list_into_weeks(self):
        """
        Take results of __generate_ful_array__ and stick it in a json dict that looks like this
        {
            "<week number>": [ "day 1 workout", "day 2 workout"]
        }
        :return:
        """

        super_dict = {}
        iterable_list = self.__generate_full_array__()
        
        week_counter = 1

        while week_counter <= self.weeks:
            day_counter = ScheduleMaker.DAYS_IN_WEEK * (week_counter - 1)
            week_list = []

            while day_counter < (ScheduleMaker.DAYS_IN_WEEK * week_counter):
                week_list.append(iterable_list[day_counter])
                day_counter += 1

            super_dict[str(week_counter)] = week_list
            week_counter += 1

        return super_dict

    def arrange_list_given_date(self, start_date):
        """
        Arrange the list given a start date
        :param start_date:
        :return: a dictionary with the structure {"date": {workout}} and an array
        of all the "date"'s returned above.
        """
        start_date = datetime.datetime.strptime(start_date, "%d-%m-%y")
        iterable_list = self.__generate_full_array__()

        return_dict = {}
        return_array = []

        for item in iterable_list:
            return_array.append(start_date.strftime("%d/%m/%y"))
            return_dict[start_date.strftime("%d/%m/%y")] = item
            start_date += datetime.timedelta(days=1)


        return return_dict, return_array

    def __generate_full_array__(self):
        """
        Take the workout_list and create a list
        """
        list = []

        i = 1

        while i <= self.workout_list_object.duration:
            workout = self.workout_list.filter(days_into_workout=i)

            if len(workout) > 1:
                raise Exception("Error. With workout set up")

            if workout:
                list.append(workout[0].workout)
            else:
                list.append(None)

            i += 1

        return list

    def get_stats(self):
        """
        Get the statistics
        :return: statistics
        """
        stats = []
        stats.append(self.get_weekly_time_stats())
        return stats

    def get_weekly_time_stats(self):
        """
        Get the weekly time statistics

        :return: the weekly time statistics. We return three things. First is
        a {"1": 90} dictionary. The second and third are the weeks and values respectively
        so we can use google visualisation tables
        """

        return_dict = {}
        return_weeks_array = []
        return_values_array = []

        array_in_weeks = self.arrange_list_into_weeks()

        for week_item in array_in_weeks:
            week_number = week_item

            total_time_for_week = 0

            for workout in array_in_weeks[week_number]:
                if workout is not None:
                    total_time_for_week += workout.total_time

            return_dict.update({week_number: total_time_for_week})
            return_weeks_array.append(week_number)
            return_values_array.append(total_time_for_week)

        return return_dict, return_weeks_array, return_values_array