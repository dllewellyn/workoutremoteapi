__author__ = 'danielllewellyn'

from rest_framework import serializers
from models import Workout, WorkoutList, StepListLink, Intensity, WorkoutListLink, StepWorkoutListLink, Keywords


class IntensitySerializer(serializers.ModelSerializer):
    """
    Intensity
    """
    class Meta:
        model = Intensity


class StepListLinkSerializer(serializers.ModelSerializer):
    """
    Step
    """
    intensity = IntensitySerializer(source="step.intensity")
    time_in_zone = serializers.ReadOnlyField(source="step.time_in_zone")
    description = serializers.ReadOnlyField(source="step.description")

    class Meta:
        model = StepListLink
        fields = ('intensity', 'time_in_zone', 'order_of_step', 'description')


class StepWorkoutListLinkSerializer(serializers.ModelSerializer):
    """
    Serializer for workout - step list linker
    """
    name = serializers.ReadOnlyField(source="step_list.name")
    step = StepListLinkSerializer(source="step_list.steplistlink_set", many=True)

    class Meta:
        model = StepWorkoutListLink
        fields = ('name', 'repetitions', 'order', 'step')


# Info about how this has been done here:
# http://stackoverflow.com/questions/17256724/include-intermediary-through-model-in-responses-in-django-rest-framework

class WorkoutListLinkSerializer(serializers.ModelSerializer):
    """
    Workout list link serializer
    """
    step_lists = StepWorkoutListLinkSerializer(source="workout.stepworkoutlistlink_set", many=True)
    overall_intensity = IntensitySerializer(source="workout.overall_intensity")
    workout_name = serializers.ReadOnlyField(source="workout.workout_name")
    cooldown = serializers.ReadOnlyField(source="workout.cooldown")
    warm_up = serializers.ReadOnlyField(source="workout.warm_up")
    total_time = serializers.ReadOnlyField(source="workout.total_time")
    
    class Meta:
        model = WorkoutListLink


class WorkoutSerializer(serializers.ModelSerializer):
    """
    Workout serializer
    """
    step_lists = StepWorkoutListLinkSerializer(source="stepworkoutlistlink_set", many=True)
    overall_intensity = IntensitySerializer()
    workout_name = serializers.ReadOnlyField()
    cooldown = serializers.ReadOnlyField()
    warm_up = serializers.ReadOnlyField()
    total_time = serializers.IntegerField()

    class Meta:
        model = Workout


class WorkoutListSerializer(serializers.ModelSerializer):
    """
    Workout list serializer
    """
    name = serializers.CharField(max_length=256)
    start_date = serializers.DateTimeField()
    duration = serializers.IntegerField()
    workouts = WorkoutListLinkSerializer(source="workoutlistlink_set", many=True)
    author = serializers.CharField(max_length=256)
    keyword = serializers.StringRelatedField(many=True)

    class Meta:
        model = WorkoutList

